import datetime
import pytz
import time
import pytest
import unittest
from app import app


# Assuming item[1] is 'TZID=America/New_York:20240626T190000'
datetime_str = 'TZID=America/New_York:20240626T190000'

# Extract the timezone and datetime string
tzid, dt_str = datetime_str.split(':')

# Parse the datetime string
dt = datetime.datetime.strptime(dt_str, "%Y%m%dT%H%M%S")

# Extract and set the timezone
tz = tzid.split('=')[1]
timezone = pytz.timezone(tz)

# Localize the datetime to the specified timezone
localized_dt = timezone.localize(dt)

# Convert to timestampz
timestamp = localized_dt.timestamp()

#val = time.mktime(datetime_str.dt.timetuple())
import unittest
from app import app  # Import your Flask app

class TestServeTestNextcloudCal(unittest.TestCase):
    def setUp(self):
        # Configure the app for testing
        app.config['TESTING'] = True
        self.client = app.test_client()

    def test_serve_test_nextcloud_cal(self):
        # Send a GET request to the /test-nextcloud-cal endpoint
        response = self.client.get('/test-nextcloud-cal')
        
        # Check the response status code
        self.assertEqual(response.status_code, 200)

        # Check that the response data contains expected calendar structure
        response_data = response.get_json()
        self.assertIn('VCALENDAR', response_data)
        self.assertIn('VEVENT', response_data['VCALENDAR'])
        
        # Verify the properties of the test event
        #event = response_data['VCALENDAR']['VEVENT'][0]
        #self.assertIn('X-TEST-PROP', event)
        #self.assertEqual(event['X-TEST-PROP'], 'tryout.')

if __name__ == '__main__':
    unittest.main()
