#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os

from flask import Flask, abort, jsonify, request
from html import unescape
from icalendar import Calendar, Event, vText
from zoneinfo import ZoneInfo
import datetime
import hashlib
import re
import requests

app = Flask(__name__)

def add_calendar_comps(data, calendar):
    events_hash_dict = {}
    for component in calendar.subcomponents:
        if not component.name in data[calendar.name]:
            data[calendar.name][component.name] = []

        comp_obj = {}
        if component.name == 'VEVENT':
            if not component['SUMMARY']:
                component['SUMMARY'] = vText("")
            if not component['DESCRIPTION']:
                component['DESCRIPTION'] = vText("")
            events = data['VCALENDAR']['VEVENT']
            unique_event_string = component['SUMMARY'] + component['DESCRIPTION']
            if unique_event_string:
                event_hash = hashlib.sha256(unique_event_string.encode('utf-8')).hexdigest()
            comp_obj['EVENT_HASH'] = event_hash
            if event_hash not in events_hash_dict:
                events_hash_dict[event_hash] = {
                    'first_event_index': len(events),
                    'events': [component['UID']]
                }

                if 'DTSTART' not in comp_obj and 'DTEND' not in comp_obj and component.get('X-MICROSOFT-CDO-ALLDAYEVENT') == 'TRUE':
                    dtstart = datetime.datetime.fromisoformat(
                        component.get('DTSTART').dt.isoformat()
                        ).replace(
                            tzinfo=ZoneInfo("America/New_York")
                            ).timestamp()
                    dtend = datetime.datetime.fromisoformat(
                        component.get('DTEND').dt.isoformat()
                        ).replace(
                            tzinfo=ZoneInfo("America/New_York")
                            ).timestamp() - 60
                    comp_obj['DTSTART'] = str(dtstart)
                    comp_obj['DTEND'] = str(dtend)
                for item in component.items():
                    if hasattr(item[1], 'dt'):
                        if component.get('X-MICROSOFT-CDO-ALLDAYEVENT') == 'TRUE':
                            continue
                        val = datetime.datetime.fromisoformat(item[1].dt.isoformat())
                        val = str(val.timestamp())
                        comp_obj[item[0]] = val
                    else:
                        if isinstance(item[1], list):
                            for item in item[1]:
                                key_string = item.params['NAME'].upper().replace(' ', '-')
                                if key_string == 'CONTACT-EMAIL':
                                    cleaned_email = re.search(r'[\w\.-]+@[\w\.-]+', item)
                                    if cleaned_email:
                                        item = cleaned_email.group()
                                comp_obj[key_string] = item
                        elif item[0] == "SEQUENCE":
                            pass
                        else:
                            val = unescape(item[1].to_ical().decode('utf-8').replace('\\,', ',').replace('\\;', ';'))
                            if(item[0]) == 'URL':
                                if val[0:4] != 'http':
                                    val = 'https://' + val
                            comp_obj[item[0]] = val
                data['VCALENDAR']['VEVENT'].append(comp_obj)
            else:
                event_index = events_hash_dict[event_hash]['first_event_index']
                dtstart = component.items().mapping['DTSTART']
                dtend = component.items().mapping['DTEND']
                startval = datetime.datetime.fromisoformat(dtstart.dt.isoformat())
                startval = str(startval.timestamp())
                endval = datetime.datetime.fromisoformat(dtend.dt.isoformat())
                endval = str(endval.timestamp())
                events[event_index]['DTSTART'] += ', ' + startval
                events[event_index]['DTEND'] += ', ' + endval
                events_hash_dict[event_hash]['events'].append(component['UID'])
                data['VCALENDAR']['VEVENT'] = events
        else:
            for item in component.items():
                if isinstance(item[1], list):
                    for item in item[1]:
                        key_string = item.params['NAME'].upper().replace(' ', '-')
                        comp_obj[key_string] = item
                else:
                    val = item[1].to_ical().decode('utf-8').replace('\\,', ',')
                    if(item[0]) == 'URL':
                        if val[0:4] != 'http':
                            val = 'https://' + val
                    comp_obj[item[0]] = val
                data[calendar.name][component.name].append(comp_obj)
    return data


@app.route('/')
def index():
    return u'Please use like <code>http://<script>document.write(location.host);</script><noscript>ical2json.pb.io</noscript>/http://www.myserver.com/path/to/file.ics</code><br>Source code and instructions at <a href="http://github.com/philippbosch/ical2json">http://github.com/philippbosch/ical2json</a>.'

@app.route('/moco-calendar')
def convert_from_url():
    urls = [
        'https://www.trumba.com/calendars/montgomery-county.ics?filter2=_277073_&filterfield2=26315', # Upcounty
        'https://www.trumba.com/calendars/montgomery-county.ics?filter2=_277076_&filterfield2=26315', # Silver Spring
        'https://www.trumba.com/calendars/montgomery-county.ics?filter2=_579062_&filterfield2=26315', # Rockville
        'https://www.trumba.com/calendars/montgomery-county.ics?filter2=_277077_&filterfield2=26315', # Bethesda/Chevy Chase
        'https://www.trumba.com/calendars/montgomery-county.ics?filter2=_277075_&filterfield2=26315', # East County
        'https://www.trumba.com/calendars/montgomery-county.ics?filter2=_277074_&filterfield2=26315'  # MidCounty
    ]
    data = {}
    for url in urls:
        if url == 'favicon.ico':
            abort(404)
        if not url.startswith('http'):
            url = 'http://%s' % url

        try:
            r = requests.get(url)
        except:
            abort(500)

        if not r.ok:
            abort(r.status_code)

        ics = r.content
        cal = Calendar.from_ical(ics)
        if cal.name not in data.keys():
            data[cal.name] = dict(cal.items())

        add_calendar_comps(data, cal)

    resp = jsonify(data)
    if 'callback' in request.args:
        resp.data = "%s(%s);" % (request.args['callback'], resp.data)
    return resp

@app.route('/test-cal')
def serve_cal():
    cal = Calendar()
    event = Event()
    event.add('X-TEST-PROP', 'tryout.', parameters={'prop1': 'val1', 'prop2': 'val2'})
    cal.add_component(event)
    data = {}
    data[cal.name] = dict(cal.items())

    for component in cal.subcomponents:
        if not component.name in data[cal.name]:
            data[cal.name][component.name] = []

        component_cal = component.to_ical().decode('utf-8').splitlines()
        comp_obj = {}
        for item in component.property_items():
            val = ""
            if isinstance(item[1], bytes):
                val = item[1].decode('utf-8').replace('\\,', ',')
            elif isinstance(item[1], vText):
                test = item[1].params
            comp_obj[item[0]] = val

        data[cal.name][component.name].append(comp_obj)

    resp = jsonify(data)
    if 'callback' in request.args:
        resp.data = "%s(%s);" % (request.args['callback'], resp.data)
    return resp

@app.route('/test-nextcloud-cal')
def serve_test_nextcloud_cal():
    urls = [
        "https://share.mayfirst.org/remote.php/dav/public-calendars/JWXyDwztjCfCejMX?export"
    ]
    data = {}
    for url in urls:
        if url == 'favicon.ico':
            abort(404)
        if not url.startswith('http'):
            url = 'http://%s' % url

        try:
            r = requests.get(url)
        except:
            abort(500)

        if not r.ok:
            abort(r.status_code)

        ics = r.content
        cal = Calendar.from_ical(ics)
        if cal.name not in data.keys():
            data[cal.name] = dict(cal.items())

        add_calendar_comps(data, cal)

    resp = jsonify(data)
    if 'callback' in request.args:
        resp.data = "%s(%s);" % (request.args['callback'], resp.data)
    return resp

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8000, debug=False)
